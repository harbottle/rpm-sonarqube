#!/bin/bash
mkdir -p ~/.config
echo -e "[copr-cli]\nlogin = ${copr_login}\nusername = harbottle\ntoken = ${copr_token}\ncopr_url = https://copr.fedorainfracloud.org" > ~/.config/copr
spec_epoch=$(rpmspec -q --srpm --qf "%{epoch}\n" ./specs/${1}.spec)
spec_version=$(rpmspec -q --srpm --qf "%{version}-%{release}\n" ./specs/${1}.spec)
[ "${spec_epoch}" = "(none)" ] || spec_version="${spec_epoch}:${spec_version}"
copr_version=$(copr-cli get-package --name ${1} --with-all-builds ${3} 2> /dev/null | jq -r '[.builds[] | select(.state =="succeeded") | select(.chroots[] | contains ("'${2}'"))] | max_by(.ended_on) | .source_package.version' 2> /dev/null )
[ "${copr_version}" = "" ] && copr_version="notfound"
echo "spec version: ${spec_version}"
echo "copr version: ${copr_version}"
[ "${spec_version}" != "${copr_version}" ]
