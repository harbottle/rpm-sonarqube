#!/bin/bash
if ./new.sh ${1} ${2} ${3}; then
  mkdir -p ./rpmbuild/{SPECS,SOURCES}
  mv -f ./specs/${1}.spec ./rpmbuild/SPECS
  mv -f ./sources/${1}/* ./rpmbuild/SOURCES || true
  spectool -g -C ./rpmbuild/SOURCES ./rpmbuild/SPECS/${1}.spec
  rpmbuild --define "_topdir $PWD/rpmbuild" --bs ./rpmbuild/SPECS/${1}.spec
fi
