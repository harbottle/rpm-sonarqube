# rpm-sonarqube

[SonarQube](https://www.sonarqube.org) RPM packages and dedicated COPR yum repo.

- [Spec files](https://gitlab.com/harbottle/harbottle-main/-/tree/master/specs)
- [COPR homepage](https://copr.fedorainfracloud.org/coprs/harbottle/sonarqube/)

## Suggested Basic Installation Procedure for CentOS 8

The following script installs PostgreSQL and the latest LTS release of
SonarQube on CentOS 8. It initializes PostgreSQL, creates a new database and
configures SonarQube to use it.

After running this script as root, SonarQube is available at
http://localhost:9000

```bash
#!/bin/bash

# SonarQube PostgreSQL password (change this)
DB_PASS=secret

# Install PostgreSQL
dnf -y install postgresql-server

# Initialize PostgreSQL
postgresql-setup --initdb

# Configure PostgreSQL client authentication using augeas
# (just edit the config file if you prefer)
dnf -y install augeas
PG_HBA=/var/lib/pgsql/data/pg_hba.conf
augtool -Ast "Pg_Hba.lns incl ${PG_HBA}" <<AUGEAS
set /files${PG_HBA}/*[type='host' and database='all' and user='all' and address='127.0.0.1/32']/method md5
set /files${PG_HBA}/*[type='host' and database='all' and user='all' and address='::1/128']/method md5
AUGEAS

# Enable and start PostgreSQL
systemctl enable --now postgresql

# Configure PostgreSQL for SonarQube
sudo su - postgres <<SU
createdb sonar
psql -c "create role sonar with login password '${DB_PASS}';"
SU

# Install SonarQube using COPR repo
dnf -y install dnf-plugins-core
dnf config-manager --add-repo https://copr.fedorainfracloud.org/coprs/harbottle/sonarqube/repo/epel-8/harbottle-sonarqube-epel-8.repo
dnf -y install sonarqube

# Configure SonarQube settings using augeas
# (just edit the config file if you prefer)
SQCONFIG=/etc/sonarqube/sonar.properties
augtool -Ast "Properties.lns incl ${SQCONFIG}" <<AUGEAS
set /files${SQCONFIG}/sonar.jdbc.username sonar
set /files${SQCONFIG}/sonar.jdbc.password '${DB_PASS}'
set /files${SQCONFIG}/sonar.jdbc.url 'jdbc:postgresql://localhost/sonar'
set /files${SQCONFIG}/sonar.web.host '127.0.0.1'
AUGEAS

# Override SonarQube service dependencies
mkdir -p /etc/systemd/system/sonarqube.service.d
cat << UNIT > /etc/systemd/system/sonarqube.service.d/10-requires.conf
[Unit]
After=postgresql.service
Requires=postgresql.service
UNIT
systemctl daemon-reload

# Enable and start SonarQube
systemctl enable --now sonarqube
```

After installing SonarQube, you can check what additional plugins are available:

```bash
dnf list available 'sonarqube*'
```

## Suggested Basic Installation Procedure for CentOS 7

The following script installs a recent version of PostgreSQL and the latest LTS
release of SonarQube on CentOS 7. It initializes PostgreSQL, creates a new
database and configures SonarQube to use it.

After running this script as root, SonarQube is available at
http://localhost:9000

```bash
#!/bin/bash

# SonarQube PostgreSQL password (change this)
DB_PASS=secret

# Install PostgreSQL 10 using software collections
yum -y install centos-release-scl
yum -y install rh-postgresql10

# Initialize PostgreSQL
scl enable rh-postgresql10 - <<SCL
postgresql-setup --initdb
SCL

# Configure PostgreSQL client authentication using augeas
# (just edit the config file if you prefer)
yum -y install augeas
PG_HBA=/var/opt/rh/rh-postgresql10/lib/pgsql/data/pg_hba.conf
augtool -Ast "Pg_Hba.lns incl ${PG_HBA}" <<AUGEAS
set /files${PG_HBA}/*[type='host' and database='all' and user='all' and address='127.0.0.1/32']/method md5
set /files${PG_HBA}/*[type='host' and database='all' and user='all' and address='::1/128']/method md5
AUGEAS

# Enable and start PostgreSQL
systemctl enable --now rh-postgresql10-postgresql

# Configure PostgreSQL for SonarQube
sudo su - postgres <<SU
scl enable rh-postgresql10 - <<SCL
createdb sonar
psql -c "create role sonar with login password '${DB_PASS}';"
SCL
SU

# Install SonarQube using COPR repo
yum -y install yum-utils
yum-config-manager --add-repo https://copr.fedorainfracloud.org/coprs/harbottle/sonarqube/repo/epel-7/harbottle-sonarqube-epel-7.repo
yum -y install sonarqube

# Configure SonarQube settings using augeas
# (just edit the config file if you prefer)
SQCONFIG=/etc/sonarqube/sonar.properties
augtool -Ast "Properties.lns incl ${SQCONFIG}" <<AUGEAS
set /files${SQCONFIG}/sonar.jdbc.username sonar
set /files${SQCONFIG}/sonar.jdbc.password '${DB_PASS}'
set /files${SQCONFIG}/sonar.jdbc.url 'jdbc:postgresql://localhost/sonar'
set /files${SQCONFIG}/sonar.web.host '127.0.0.1'
AUGEAS

# Override SonarQube service dependencies
mkdir -p /etc/systemd/system/sonarqube.service.d
cat << UNIT > /etc/systemd/system/sonarqube.service.d/10-requires.conf
[Unit]
After=rh-postgresql10-postgresql.service
Requires=rh-postgresql10-postgresql.service
UNIT
systemctl daemon-reload

# Enable and start SonarQube
systemctl enable --now sonarqube
```

After installing SonarQube, you can check what additional plugins are available:

```bash
yum list available 'sonarqube*'
```